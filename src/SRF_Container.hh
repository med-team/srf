//Copyright 2007 Asim Siddiqui
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
#ifndef SRF_CONTAINER_HH
#define SRF_CONTAINER_HH
//
// file: SRF_Container.hh
// Purpose: Header file for container class
// Author: Asim Siddiqui
// Update History
// 18th March 2007  Asim Siddiqui        created
//
#include <ios>
#include <fstream>
#include <vector>
#include <string>
#include <io_lib/ztr.h>
#include <SRF_util.hh>

class SRF_ReadSet;

class SRF_Container
{
    public:
        SRF_Container( void );
        ~SRF_Container( void );

        bool populate( std::fstream& file );

        bool setup( const std::string& baseCallerIn,
                    const std::string& baseCallerVersionIn );

        bool write( std::fstream& file );

        bool writeReadSet( std::fstream& file,
                           const std::string& ztrHeaderBlob,
                           const std::vector<ztr_t>& ztrBlobs,
                           const std::vector<std::string>& readIds,
                           const std::string& uniqueIdPrefix );

// get methods for private variables
        const std::string& getBaseCaller( void );
        const std::string& getBaseCallerVersion( void );
        char getContainerType( void );
        const std::string& getHeaderBlockType( void );
        const std::string& getFormatVersion( void );

// methods to position file pointer at readsets for the container
        bool filePtrToContainerStart( std::fstream& file );
        bool filePtrToContainerEnd( std::fstream& file );
        bool getReadSet( std::fstream& file );
        bool nextReadSet( std::fstream& file );
        bool previousReadSet( std::fstream& file );
        ADOPT SRF_ReadSet* firstReadSet( std::fstream& file );

        void dump( void );

    private:
        std::string xmlString;
        std::string baseCaller;
        std::string baseCallerVersion;
        char containerType;
        std::string headerBlockType;
        std::string formatVersion;

        std::streampos containerPositionInFile;
        off_t sizeContainerHeader;
};

#endif
