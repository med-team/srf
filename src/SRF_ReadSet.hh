//Copyright 2007 Asim Siddiqui
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

#ifndef SRF_READSET_HH
#define SRF_READSET_HH
//
// file: SRF_ReadSet.hh
// Purpose: Header file for container class
// Author: Asim Siddiqui
// Update History
// 15th April 2007  Asim Siddiqui        created
//
#include <fstream>
#include <string>
#include <io_lib/ztr.h>
#include <SRF_Read.hh>

class SRF_ReadSet
{
    public:
        SRF_ReadSet( void );
        ~SRF_ReadSet( void );

        bool readSetStart( std::fstream& file );
        bool readSetReadStart( std::fstream& file );
        bool populate( std::fstream& file );
        ADOPT SRF_Read* getRead( std::fstream& file );
        bool setup( const std::string& uniqueIdPrefixIn, const std::string& ztrHeaderIn );
        bool write( std::fstream& file, const std::vector<ztr_t>& ztrBlobs, const std::vector<std::string>& readIds );

        void dump( void );

    private:
        std::string uniqueIdPrefix;
        std::streampos positionInFile;
        off_t blockSize;
        char subBlockType;

        std::string ztrHeader;
};

#endif
