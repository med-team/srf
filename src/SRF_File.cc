//Copyright 2007 Asim Siddiqui
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//
#include <ios>
#include <iostream>
#include <fstream>
#include <SRF_File.hh>
#include <SRF_util.hh>
#include <SRF_Container.hh>

SRF_File::SRF_File( const char* filename, openType openParam )
{
    if ( openParam == SRF_File::openTypeRead )
    {
        file.open(filename, std::ios::in|std::ios::binary );
    }
    else if ( openParam == SRF_File::openTypeWrite )
    {
        file.open(filename, std::ios::out|std::ios::binary );
    }
    else if ( openParam == SRF_File::openTypeReadWrite )
    {
        file.open(filename, std::ios::in|std::ios::out|std::ios::binary );
    }

    if ( !file.is_open() )
    {
        std::string errMesg = "can't open file ";
        errMesg.append( filename );
        SRF_ReportError( errMesg );
    }
}


SRF_File::~SRF_File( void )
{
    file.close();
}

ADOPT SRF_Container*
SRF_File::getCurrentContainer()
{
    SRF_Container* container = new SRF_Container;
    if ( !container->populate( file ) )
    {
        SRF_ReportError( "can't read container" );
        return FALSE;
    }

    return container;
}
    
bool
SRF_File::firstContainer( void )
{
    file.seekg( 0 );
    return TRUE;
}

bool
SRF_File::nextContainer( void )
{
    SRF_ReportError( "Not implemented" );
    return FALSE;
}

bool
SRF_File::previousContainer( void )
{
    SRF_ReportError( "Not implemented" );
    return FALSE;
}

std::fstream&
SRF_File::getFile( void )
{
    return file;
}

