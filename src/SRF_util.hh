//Copyright 2007 Asim Siddiqui
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

#ifndef SRF_UTIL_HH
#define SRF_UTIL_HH
//
// file: SRF_util.h
// Purpose: Header file for container writer class
// Author: Asim Siddiqui
// Update History
// 18th March 2007  Asim Siddiqui        created
//

#define FALSE 0
#define TRUE 1
#define ABANDON
#define ADOPT

#include <ios>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <io_lib/ztr.h>
#include <io_lib/Read.h>

typedef struct SRF_BasePrb
{
    int a_prb;
    int c_prb;
    int g_prb;
    int t_prb;
};

ADOPT char* SRF_cStrToPascalStr( const char* str );
std::string SRF_readString( std::fstream& input );
void SRF_ReportError( const std::string& errMesg );

Read *SRF_create_read(const char *seq, std::vector<SRF_BasePrb> prbs );

ADOPT ztr_t*
SRF_output_ztr( Read *r);


#endif
