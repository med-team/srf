//Copyright 2007 Asim Siddiqui
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

#ifndef SRF_READ_HH
#define SRF_READ_HH
//
// file: SRF_Read.hh
// Purpose: Header file for container class
// Author: Asim Siddiqui
// Update History
// 15th April 2007  Asim Siddiqui        created
//
#include <fstream>
#include <string>
#include <io_lib/ztr.h>

class SRF_Read
{
    public:
        SRF_Read( void );
        SRF_Read( const std::string* shortId, const ztr_t* ztr );
        ~SRF_Read( void );

        int populate( std::string& ztrHeader, std::fstream& file );
        int write( std::fstream& file );

        void dump( void );

    private:
        std::string shortId;
        off_t positionInFile;
        ztr_t* ztr;
        const ztr_t* ztrConst;
        off_t blockSize;
        char readFlags;
};

#endif
