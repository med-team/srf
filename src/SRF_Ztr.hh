//Copyright 2007 Asim Siddiqui
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//

//
#include <ios>
#include <sstream>
#include <io_lib/ztr.h>

static int ztr_read_header(std::stringstream& ztrStr, ztr_header_t *h);
static ztr_chunk_t *ztr_read_chunk_hdr( std::stringstream& ztrStr );
ztr_t *read_ztr( std::stringstream& ztrStr );
uint32_t SRF_calcSizeZTR( const ztr_t* ztr );
int SRF_writeZtr( const ztr_t* ztr, std::fstream& file );
