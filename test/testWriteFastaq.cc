//
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stddef.h>
#include <SRF_util.hh>
#include <SRF_File.hh>
#include <SRF_ReadSet.hh>
#include <SRF_Container.hh>

using namespace std;

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <io_lib/Read.h>
#include <io_lib/misc.h>
#include <io_lib/hash_table.h>
#include <io_lib/ztr.h>
#include <zlib.h>

main(int argc, char* argv[])
{
    if ( argc != 3 )
    {
        cout << "Usage: fastaWriter <fasta file> <output file>\n";
        exit(1);
    }

    SRF_File::openType mode = SRF_File::openTypeWrite;
    SRF_File file( argv[2], mode );

    SRF_Container container;

    container.setup( "Test",
                     "0.0" );

    container.write( file.getFile() );

    SRF_ReadSet newReadSet;
    newReadSet.setup( "Test",  "" );

    int readIdCounter = 0;

    int a,b,c,d;
    string str, seq;

    std::vector<ztr_t> ztrBlobs;
    std::vector<SRF_BasePrb> prbs;
    std::vector<std::string> readIds;
// open fasta
    std::ifstream fastaInput;
    fastaInput.open(argv[1], std::ios::in );

    int where = 0;
    string readId;
    while( fastaInput.good() )
    {
        fastaInput >> str;
        if ( str[0] == '@' )
        {
            str.erase( 0, 1 );
            readId = str;
            where = 0;
        }
        else if ( str[0] == '+' )
        {
            where = 1;
        }
        else if ( where == 0 )
        {
            seq = str;
        }
        else if ( where == 1 )
        {
            SRF_BasePrb singlePrb;
            int ii = 0;
            while (ii < seq.length() )
            {
                singlePrb.a_prb = 0;
                singlePrb.c_prb = 0;
                singlePrb.g_prb = 0;
                singlePrb.t_prb = 0;
                int prb = int( str[ii + 1 ] ) - 33 ;
//                int prb = int( str[ii + 1 ] );
                switch( seq[ii] )
                {
                    case 'A':
                        singlePrb.a_prb = prb;
                        break;
                    case 'C':
                        singlePrb.c_prb = prb;
                        break;
                    case 'G':
                        singlePrb.g_prb = prb;
                        break;
                    case 'T':
                        singlePrb.t_prb = prb;
                        break;
                }
                prbs.push_back( singlePrb );

                ii++;
            }
            Read* read = SRF_create_read( seq.c_str(), prbs );
            ztr_t* ztr = NULL;
            ztr = SRF_output_ztr( read );

            cout << readId << endl;
            ztrBlobs.push_back( *ztr );
            readIds.push_back( readId );
        }
    }
    newReadSet.write( file.getFile(), ztrBlobs, readIds );
}

