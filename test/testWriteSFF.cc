//
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stddef.h>
#include <SRF_util.hh>
#include <SRF_File.hh>
#include <SRF_ReadSet.hh>
#include <SRF_Container.hh>

using namespace std;

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <io_lib/Read.h>
#include <io_lib/misc.h>
#include <io_lib/hash_table.h>
#include <io_lib/ztr.h>
#include <io_lib/sff.h>
#include <zlib.h>

template <class T>
std::string SRF_to_string(T t, std::ios_base & (*f)(std::ios_base&))
{
  std::ostringstream oss;
  oss << f << t;
  return oss.str();
}   

main(int argc, char* argv[])
{
    if ( argc != 3 )
    {
        cout << "Usage: sffWriter <SFF file> <output file>\n";
        exit(1);
    }

    SRF_File::openType mode = SRF_File::openTypeWrite;
    SRF_File file( argv[2], mode );

    SRF_Container container;

    container.setup( "Test",
                     "0.0" );

    container.write( file.getFile() );

    SRF_ReadSet newReadSet;
    newReadSet.setup( "Test", "" );

    int readIdCounter = 0;

    int a,b,c,d;
    string str, seq;

    std::vector<ztr_t> ztrBlobs;
    std::vector<std::string> readIds;
// open fasta
//    std::ifstream fastaInput;
//    fastaInput.open(argv[1], std::ios::in );
    Read* read = NULL;
    mFILE* mf = mfopen(argv[1], "r");
    while ( ( read = mfread_sff( mf ) ) != NULL )
    {
        string readId;
        readId = SRF_to_string<int>(readIdCounter, std::dec);
        ztr_t* ztr = NULL;
        ztr = SRF_output_ztr( read );
        ztrBlobs.push_back( *ztr );
        readIds.push_back( readId );
        readIdCounter++;
    }
    newReadSet.write( file.getFile(), ztrBlobs, readIds );
}

