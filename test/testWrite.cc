//
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stddef.h>
#include <SRF_util.hh>
#include <SRF_File.hh>
#include <SRF_ReadSet.hh>
#include <SRF_Container.hh>

using namespace std;

template <class T>
std::string SRF_to_string(T t, std::ios_base & (*f)(std::ios_base&))
{
  std::ostringstream oss;
  oss << f << t;
  return oss.str();
}   

main(int argc, char **argv)
{
    SRF_File::openType mode = SRF_File::openTypeWrite;
    SRF_File file( "myFirstSSRFFile", mode );
    ifstream infile;
    SRF_Container container;

    if ( argc != 2 )
    {
	std::cout << "Usage: writer <input file>\n";
	exit(1);
    }


    infile.open (argv[1], ifstream::in);

    container.setup( "Test",
                     "0.0" );

    container.write( file.getFile() );

    SRF_ReadSet newReadSet;
    newReadSet.setup( "Test", "" );

    int readIdCounter = 0;

    int a,b,c,d;
    string str;
    std::vector<SRF_BasePrb> prbs;

    std::vector<ztr_t> ztrBlobs;
    std::vector<std::string> readIds;
    while( infile.good() )
    {
        infile >> a >> skipws >> b >> skipws >> c >> skipws >> d >> skipws >> str; 
        Read* read = SRF_create_read( str.c_str(), prbs );
        ztr_t* ztr = NULL;
        ztr = SRF_output_ztr( read );

        string readId;
        readId = SRF_to_string<int>(readIdCounter, std::dec);
        ztrBlobs.push_back( *ztr );
        readIds.push_back( readId );
        readIdCounter++;

	read_deallocate(read);
    }
    newReadSet.write( file.getFile(), ztrBlobs, readIds );

    /* FIXME: memory leak on ztr here still */

    infile.close();

    return 0;
}

