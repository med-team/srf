//
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <stddef.h>
#include <SRF_Container.hh>
#include <SRF_ReadSet.hh>
#include <SRF_Read.hh>
#include <SRF_File.hh>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <io_lib/Read.h>
#include <io_lib/misc.h>
#include <io_lib/hash_table.h>
#include <io_lib/ztr.h>
#include <zlib.h>

template <class T>
std::string to_string(T t, std::ios_base & (*f)(std::ios_base&))
{
  std::ostringstream oss;
  oss << f << t;
  return oss.str();
}


main(int argc, char* argv[])
{
    if ( argc != 2 )
    {
        std::cout << "Usage: reader <input file>\n";
        exit(1);
    }

    SRF_File::openType mode = SRF_File::openTypeRead;
    SRF_File file( argv[1], mode );

    SRF_Container* container = file.getCurrentContainer();

    if ( container == NULL )
    {
        SRF_ReportError( "error opening container" );
        exit (1);
    }

    container->dump();

    SRF_ReadSet* readSet = container->firstReadSet( file.getFile() );

    if ( readSet == NULL )
    {
        SRF_ReportError( "error opening readset" );
        exit (1);
    }

    readSet->dump();
    
    SRF_Read* read = NULL;
    while ( ( read = readSet->getRead( file.getFile() )) != NULL )
    {
        read->dump();
    }
}
